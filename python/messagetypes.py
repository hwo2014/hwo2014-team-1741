MSG_JOIN = 'join'
MSG_YOUR_CAR = 'yourCar'
MSG_GAME_INIT = 'gameInit'
MSG_GAME_START = 'gameStart'
MSG_CAR_POSITIONS = 'carPositions'
MSG_THROTTLE = 'throttle'
MSG_GAME_END = 'gameEnd'
MSG_TOURNAMENT_END = 'tournamentEnd'
MSG_CRASH = 'crash'
MSG_SPAWN = 'spawn'
MSG_LAP_FINISHED = 'lapFinished'
MSG_DNF = 'dnf'
MSG_FINISH = 'finish'
MSG_SWITCH_LANE = 'switchLane'
MSG_CREATE_RACE = 'createRace'
MSG_JOIN_RACE = 'joinRace'
MSG_PING = 'ping'
MSG_ERROR = 'error'
MSG_TURBO_AVAILABLE = 'turboAvailable'
MSG_TURBO = 'turbo'
MSG_TURBO_START = 'turboStart'
MSG_TURBO_END = 'turboEnd'