import json
import itertools
from math import pi

from carmanager import CarManager
from gamemanager import GameManager

class TrackLayout(object):
    def __init__(self):
        self.car_manager = CarManager()
        self.game_manager = GameManager()
        self.lane_switching_map = {}
        self.groups = []
        self.is_qualifying = False

    def parse_track_data(self, data):
        track_info = data['race']['track']
        car_info = data['race']['cars']
        race_session_info = data['race']['raceSession']

        self.id = track_info.get('id')
        self.name = track_info.get('name')
        self.pieces = self.get_track_pieces(track_info.get('pieces'))
        self.lanes = self.get_track_lanes(track_info.get('lanes'))

        # qualifying round has a different race session object
        if race_session_info.has_key('durationMs'): # this is the qualifying session object
            self.race_duration_ms = race_session_info['durationMs']
            self.laps = -1
            self.max_lap_time_ms = 0
            self.quick_race = False
            self.is_qualifying = True
        else:
            self.laps = race_session_info.get('laps')
            self.max_lap_time_ms = race_session_info.get('maxLapTimeMs')
            self.quick_race = race_session_info.get('quickRace')
            self.race_duration_ms = 0
            self.is_qualifying = False
        
        self.car_manager.fill_car_info(car_info)
        self.analyse_track()

    def analyse_track(self):
        last_piece = None
        for k, g in itertools.groupby(self.pieces, lambda x: ','.join([str(x.length), str(x.angle), str(x.radius)])):
            self.groups.append([p.index for p in list(g)])
        print self.groups

        # do switch if the switching makes the path somewhat straight
        pieces_with_switch = itertools.ifilter(lambda x: x.switch, self.pieces)
        for piece in pieces_with_switch:
            next_index = piece.index + 1 if piece.index + 1 < len(self.pieces) else 0
            previous_idx = piece.index - 1 if piece.index - 1 >= 0 else len(self.pieces) - 1
            # if ((self.pieces[next_index].is_curved() or self.pieces[next_index].is_sharp_curved()) and (self.pieces[previous_idx].is_curved() or self.pieces[previous_idx].is_sharp_curved())):
            if self.pieces[next_index].is_narrow():
                self.lane_switching_map[previous_idx] = 'Left' if self.pieces[next_index].angle > 0 else 'Right'
            else:
                self.lane_switching_map[previous_idx] = 'Right' if self.pieces[next_index].angle > 0 else 'Left'

            if self.pieces[previous_idx].is_sharp_curved():
                if self.lane_switching_map.has_key(previous_idx):
                    del self.lane_switching_map[previous_idx]
        print self.lane_switching_map

    def has_switch(self, piece_group):
        for piece in piece_group:
            if piece.switch:
                return piece.index
        return -1

    def get_track_pieces(self, track_pieces):
        pieces = []
        for index, track_piece in enumerate(track_pieces):
            length = track_piece.get('length', 0)
            switch = track_piece.get('switch', False)
            radius = track_piece.get('radius', 0)
            angle = track_piece.get('angle', 0)

            pieces.append(TrackPiece(index, length, switch, angle, radius))
        return pieces

    def get_track_lanes(self, track_lanes):
        lanes = []
        for lane in track_lanes:
            index = lane.get('index', -1)
            distance = lane.get('distanceFromCenter', 0)
            lanes.append(TrackLane(index, distance))
        return lanes

    def set_my_car(self, data):
        self.car_manager.set_my_car(data)

    def update_car_positions(self, car_data, game_id, game_tick):
        if self.game_manager.check_and_update_game_id(game_id, game_tick):
            self.car_manager.update_car_info(car_data)
            return self.game_manager.calculate_throttle(self, self.car_manager)
        else:
            print 'Unknown game id received: %s'%(game_id)
            return 0.5  #default throttle

    def handle_car_crash(self, car_data, game_tick):
        self.car_manager.handle_car_crash(car_data, game_tick)

        # record crash if my car crashed
        car_id = (car_data['name'], car_data['color'])
        if car_id == self.car_manager.my_car:
            self.game_manager.record_crash()

    def handle_car_spawn(self, car_data, game_tick):
        self.car_manager.handle_car_spawn(car_data, game_tick)

        if self.game_manager.in_trubo:
            self.game_manager.reset_turbo_crash = True

    def handle_car_dnf(self, car_data, game_tick):
        self.car_manager.handle_car_dnf(car_data, game_tick)

    def handle_car_finished(self, car_data, game_tick):
        self.car_manager.handle_car_finished(car_data, game_tick)

    def handle_turbo(self, turbo_data, game_tick):
        self.game_manager.handle_turbo(turbo_data, game_tick)

    def handle_lap_finished(self, data, game_tick):
        self.car_manager.handle_lap_finished(data, game_tick)

    def get_piece_length(self, piece_index):
        track_piece = self.pieces[piece_index]
        if track_piece.length == 0:
            return (abs(track_piece.angle) / 360) * 2 * pi * track_piece.radius
        else:
            return track_piece.length

    def get_separation_dist_by_lane(self, lane_index):
        distance = 0
        for lane in self.lanes:
            if lane.index == lane_index:
                distance = lane.distance_from_center
                break
        return distance

    def get_group_size(self, piece_index):
        for g in self.groups:
            if piece_index in g:
                return len(g)
        return 0

    def get_group(self, piece_index):
        for g in self.groups:
            if piece_index in g:
                return g
        return []

    def get_group_total_angle(self, piece_index):
        group = self.get_group(piece_index)
        total_angle = 0
        for ele in group:
            total_angle += abs(self.pieces[ele].angle)
        return total_angle

    def update_turbo_status(self, value):
        self.game_manager.update_turbo_status(value)

class TrackPiece(object):
    def __init__(self, index, length, switch, angle, radius):
        self.index = index
        self.length = length
        self.switch = switch
        self.angle = angle
        self.radius = radius

    def is_straight(self):
        return self.length > 0 and self.radius == 0

    def is_curved(self):
        return abs(self.angle) > 0 and abs(self.angle) <= 25 and self.length == 0

    def is_sharp_curved(self):
        return abs(self.angle) > 25 and self.length == 0

    def is_narrow(self):
        return abs(self.radius) <= 50 and abs(self.angle) != 0

    def is_wide(self):
        return abs(self.radius) >= 150 and abs(self.angle) != 0

    def __str__(self):
        return 'Track Piece=>Index: %s, Length: %s, Switch: %s, Angle: %s, Radius: %s'%(self.index, self.length, self.switch, self.angle, self.radius)


class TrackLane(object):
    def __init__(self, index, distance_from_center):
        self.index = index
        self.distance_from_center = distance_from_center

    def __str__(self):
        return 'Track Lane=> Index: %s, DistanceFromCenter: %s'%(self.index, self.distance_from_center)