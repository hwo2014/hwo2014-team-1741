import itertools
from copy import deepcopy
from math import sqrt

MAX_THROTTLE = 1
MAX_ANGLE_CHANGE = 5
CONSTANT = 0.5
GRAVITY = 0.098
MIN_DISTANCE_TO_CAR = 3

# piece transitions
T_INVALID = 0
T_STRAIGHT_TO_STRAIGHT = 1
T_STARIGHT_TO_CURVE = 2
T_STRAIGHT_TO_SHARP_CURVE = 3
T_CURVE_TO_CURVE = 4
T_CURVE_TO_STRAIGHT = 5
T_CURVE_TO_SHARP_CURVE = 6
T_SHARP_CURVE_TO_SHARP_CURVE = 7
T_SHARP_CURVE_TO_CURVE = 8
T_SHARP_CURVE_TO_STRAIGHT = 9

class GameManager(object):
    def __init__(self, game_id = '', tick = 0):
        self.game_id = game_id
        self.current_tick = tick
        self.throttle = 1
        self.apply_turbo = False
        self.my_car_copy = None
        self.next_piece = None
        self.transition = T_INVALID
        self.invalidate_transition = False
        self.switch_pending = ''
        self.target_throttle = {}
        self.slow_down_at_piece = -1
        self.long_straight_road = False
        self.turbo_info = ()
        self.crash_history = {}
        self.in_trubo = False
        self.reset_turbo_crash = False
        self.slip_angle = 60

    def check_and_update_game_id(self, game_id, tick):
        if len(self.game_id) == 0:
            self.game_id = game_id
            self.current_tick = tick
            return True
        elif self.game_id == game_id:
            self.current_tick = tick
            return True
        else:
            return False

    def calculate_throttle(self, track_layout, car_manager):
        my_car = car_manager.find_my_car()

        # ignore the first tick. start with default throttle
        if self.my_car_copy is not None:
            is_piece_changed = my_car.car_info.piece_index != self.my_car_copy.car_info.piece_index
            if is_piece_changed:
                self.on_piece_changed(track_layout, my_car)

            # handle dynamic switching
            self.handle_dynamic_switching(track_layout, car_manager)

            # apply pending switch
            if len(self.switch_pending) > 0 and self.is_piece_length_travelled(track_layout, my_car, 0.8):
                switch_val = 2 if self.switch_pending == 'Right' else -1
                print 'Switching Lane: %s'%(self.switch_pending)
                self.switch_pending = ''
                return switch_val

            # apply transition
            if self.transition != T_INVALID:
                self.apply_transition(track_layout, my_car);

            # apply turbo if applicable
            if self.apply_turbo and len(self.turbo_info) > 0 and self.turbo_info[3] and self.is_piece_length_travelled(track_layout, my_car, 0.2):
                if abs(my_car.car_info.angle) <= 40:
                    print 'Applying TURBO'
                    self.turbo_info = (self.turbo_info[0], self.turbo_info[1], self.current_tick, False)
                    self.apply_turbo = False
                    return 100
        else:
            self.init_throttle(track_layout, my_car)
            self.on_piece_changed(track_layout, my_car)

        # copy current car info to a local variable
        self.my_car_copy = deepcopy(my_car)

        # clamp the throttle value to its limits
        self.throttle = min(self.throttle, MAX_THROTTLE)
        self.throttle = max(self.throttle, 0)

        # decelerate when in turbo
        if self.in_trubo and self.turbo_ticks_elapsed(0.75):
            if self.reset_turbo_crash:
                self.throttle = 1   # if crashed while in turbo mode start with full throttle
            else:
                self.throttle = 0.01

        # print 'Throttle: %s, Angle: %s'%(self.throttle, my_car.car_info.angle)
        return self.throttle

    def apply_transition(self, track_layout, my_car):
        if self.transition == T_STRAIGHT_TO_STRAIGHT:
            self.handle_transition_straight_to_straight(track_layout, my_car)
        elif self.transition == T_STARIGHT_TO_CURVE:
            self.handle_transition_straight_to_curve(track_layout, my_car)
        elif self.transition == T_STRAIGHT_TO_SHARP_CURVE:
            self.handle_transition_straight_to_sharp_curve(track_layout, my_car)
        elif self.transition == T_CURVE_TO_CURVE:
            self.handle_transition_curve_to_curve(track_layout, my_car)
        elif self.transition == T_CURVE_TO_STRAIGHT:
            self.handle_transition_curve_to_straight(track_layout, my_car)
        elif self.transition == T_CURVE_TO_SHARP_CURVE:
            self.handle_transition_curve_to_sharp_curve(track_layout, my_car)
        elif self.transition == T_SHARP_CURVE_TO_SHARP_CURVE:
            self.handle_transition_sharp_curve_to_sharp_curve(track_layout, my_car)
        elif self.transition == T_SHARP_CURVE_TO_CURVE:
            self.handle_transition_sharp_curve_to_curve(track_layout, my_car)
        elif self.transition == T_SHARP_CURVE_TO_STRAIGHT:
            self.handle_transition_sharp_curve_to_straight(track_layout, my_car)

    def handle_transition_straight_to_straight(self, track_layout, my_car):
        deceleration = self.apply_third_law_of_motion(track_layout, my_car)
        self.throttle = self.throttle + deceleration

    def handle_transition_straight_to_curve(self, track_layout, my_car):
        aceleration = self.apply_third_law_of_motion(track_layout, my_car)
        self.throttle = self.throttle + aceleration

    def handle_transition_straight_to_sharp_curve(self, track_layout, my_car):
        deceleration = self.apply_third_law_of_motion(track_layout, my_car)
        self.throttle = self.throttle + deceleration

    def handle_transition_curve_to_curve(self, track_layout, my_car):
        aceleration = self.apply_third_law_of_motion(track_layout, my_car)
        self.throttle = self.throttle + aceleration

    def handle_transition_curve_to_straight(self, track_layout, my_car):
        self.full_throttle()

    def handle_transition_curve_to_sharp_curve(self, track_layout, my_car):
        deceleration = self.apply_third_law_of_motion(track_layout, my_car)
        self.throttle = self.throttle + deceleration

    def handle_transition_sharp_curve_to_sharp_curve(self, track_layout, my_car):
        aceleration = self.apply_third_law_of_motion(track_layout, my_car)
        self.throttle = self.throttle + aceleration

    def handle_transition_sharp_curve_to_curve(self, track_layout, my_car):
        if not self.is_piece_length_travelled(track_layout, my_car, 0.1):
            return

        aceleration = self.apply_third_law_of_motion(track_layout, my_car)
        self.throttle = self.throttle + aceleration

    def handle_transition_sharp_curve_to_straight(self, track_layout, my_car):
        if not self.is_piece_length_travelled(track_layout, my_car, 0.1):
            return

        aceleration = self.apply_third_law_of_motion(track_layout, my_car)
        self.throttle = self.throttle + abs(aceleration)

    def full_throttle(self):
        self.throttle = MAX_THROTTLE

    def apply_third_law_of_motion(self, track_layout, my_car):
        current_piece_index = my_car.car_info.piece_index
        current_piece = track_layout.pieces[current_piece_index]
        required_throttle = self.target_throttle[self.next_piece.index]
        distance_to_cover = track_layout.get_piece_length(current_piece.index) - my_car.car_info.piece_distance
        deceleration =  (pow(round(required_throttle, 2), 2) - pow(round(self.throttle, 2), 2)) / (2 * (distance_to_cover / float(100)))
        return deceleration

    def is_next_sharp_curve_opposite(self, track_layout, my_car):
        current_piece = track_layout.pieces[my_car.car_info.piece_index]
        if current_piece.is_sharp_curved() and self.next_piece.is_sharp_curved():
            if (current_piece.angle > 0 and self.next_piece.angle < 0) or (current_piece.angle < 0 and self.next_piece.angle > 0):
                return True
        return False

    def on_piece_changed(self, track_layout, my_car):
        current_piece_index = my_car.car_info.piece_index
        current_piece = track_layout.pieces[current_piece_index]
        next_piece_index = current_piece_index + 1 if current_piece_index < len(track_layout.pieces) - 1 else 0 
        self.next_piece = track_layout.pieces[next_piece_index]
        self.invalidate_transition = True

        if self.next_piece.is_straight():
            self.check_long_straight_road(track_layout)
            if self.long_straight_road and next_piece_index == self.slow_down_at_piece + 1:
                self.target_throttle[next_piece_index] = self.get_target_throttle(track_layout, my_car, next_piece_index + 1) * 2
            else:
                self.target_throttle[next_piece_index] = 0 if self.next_piece.index == 0 else MAX_THROTTLE
        elif self.next_piece.is_curved():
            self.target_throttle[next_piece_index] = self.get_target_throttle(track_layout, my_car)
        elif self.next_piece.is_sharp_curved():
            self.target_throttle[next_piece_index] = self.get_target_throttle(track_layout, my_car)
            if self.long_straight_road:
                self.slow_down_at_piece = -1
                self.long_straight_road = False

        print 'Target Throttle: %s, for piece: %s'%(self.target_throttle[next_piece_index], self.next_piece)
        if track_layout.lane_switching_map.has_key(current_piece_index):
            self.switch_pending = track_layout.lane_switching_map[current_piece_index]

        self.change_transtion(track_layout.pieces[my_car.car_info.piece_index])

    def get_target_throttle(self, track_layout, my_car, piece_index = -1):
        distance_from_center = track_layout.get_separation_dist_by_lane(my_car.car_info.end_lane)
        target_piece = self.next_piece if piece_index == -1 else track_layout.pieces[piece_index]
        radius = distance_from_center + target_piece.radius

        # if this is the last piece of the track return 0
        if target_piece.index == 0:
            return 0

        group_angle = track_layout.get_group_total_angle(target_piece.index)
        max_throttle = max(sqrt((group_angle / float(360)) * GRAVITY * (radius / float(100))), 0)
        multiplier = 1
        if group_angle <= 90:
            if target_piece.is_narrow():
                multiplier = 2.5
            elif not target_piece.is_narrow() and not target_piece.is_wide():
                multiplier = 1.25
            else:
                multiplier = 1.5
            max_throttle = max_throttle * (360.0 / (group_angle * multiplier))
        elif group_angle >= 180:
            # add throttle if you are in the last piece of the group
            group = track_layout.get_group(target_piece.index)
            if target_piece.index == group[len(group) - 1]:
                max_throttle = max_throttle + (target_piece.index - group[1]) / float(len(group))

        print 'Throttle Limit: %s, for group angle: %s'%(max_throttle, group_angle)
        return max_throttle

    def check_long_straight_road(self, track_layout):
        # if a long road has already been identified, dont do anything until we reach the end of long road
        if self.long_straight_road:
            return

        count = 1
        total_length = 0
        start_index = self.next_piece.index
        while count < len(track_layout.pieces):
            lookup_index = start_index + count
            if lookup_index >= len(track_layout.pieces):
                lookup_index = lookup_index - len(track_layout.pieces)
            if track_layout.pieces[lookup_index].is_straight():
                count = count + 1
                total_length += track_layout.pieces[lookup_index].length
            else:
                if track_layout.pieces[lookup_index].is_sharp_curved() and count > 4:
                    self.long_straight_road = True
                    self.slow_down_at_piece = lookup_index - 2
                    self.apply_turbo = self.can_apply_turbo(count, total_length)
                    return
                elif track_layout.pieces[lookup_index].is_sharp_curved() and track_layout.pieces[lookup_index].is_narrow() and track_layout.get_group_size(lookup_index) >= 4:
                    self.long_straight_road = True
                    self.slow_down_at_piece = lookup_index - 2
                    return
                elif track_layout.pieces[lookup_index].is_sharp_curved() and track_layout.pieces[lookup_index].is_narrow() and track_layout.get_group_size(lookup_index) == 2:
                    # HACK
                    self.long_straight_road = True
                    self.slow_down_at_piece = lookup_index - 2
                    return
                else:
                    break
        self.long_straight_road = False

    def init_throttle(self, track_layout, my_car):
        current_piece_index = my_car.car_info.piece_index
        current_piece = track_layout.pieces[current_piece_index]
        if current_piece.is_sharp_curved():
            self.throttle = self.throttle * 0.5
        elif current_piece.is_curved():
            self.throttle = self.throttle * 0.7

    def change_transtion(self, current_piece):
        # set the correct incoming transition
        # transiton from a straight piece
        if current_piece.is_straight() and self.next_piece.is_straight():
            self.transition = T_STRAIGHT_TO_STRAIGHT
        elif current_piece.is_straight() and self.next_piece.is_curved():
            self.transition = T_STARIGHT_TO_CURVE
        elif current_piece.is_straight() and self.next_piece.is_sharp_curved():
            self.transition = T_STRAIGHT_TO_SHARP_CURVE
        # transiton from a curved piece
        elif current_piece.is_curved() and self.next_piece.is_curved():
            self.transition = T_CURVE_TO_CURVE
        elif current_piece.is_curved() and self.next_piece.is_straight():
            self.transition = T_CURVE_TO_STRAIGHT
        elif current_piece.is_curved() and self.next_piece.is_sharp_curved():
            self.transition = T_CURVE_TO_SHARP_CURVE
        # transiton from a sharp curved piece
        elif current_piece.is_sharp_curved() and self.next_piece.is_straight():
            self.transition = T_SHARP_CURVE_TO_STRAIGHT
        elif current_piece.is_sharp_curved() and self.next_piece.is_curved():
            self.transition = T_SHARP_CURVE_TO_CURVE
        elif current_piece.is_sharp_curved() and self.next_piece.is_sharp_curved():
            self.transition = T_SHARP_CURVE_TO_SHARP_CURVE
        else:
            self.transition = T_INVALID

        print 'Current Transition: %s'%(self.transition)

    def is_piece_length_travelled(self, track_layout, my_car, travelled_percentage):
        total_piece_length = track_layout.get_piece_length(my_car.car_info.piece_index)
        piece_travelled = my_car.car_info.piece_distance
        return piece_travelled >= total_piece_length * travelled_percentage

    def handle_turbo(self, turbo_data, game_tick):
        self.turbo_info = (turbo_data['turboDurationTicks'], turbo_data['turboFactor'], game_tick, True)

    def can_apply_turbo(self, piece_count, total_length):
        if len(self.turbo_info) == 0 or (len(self.turbo_info) > 0 and self.turbo_info[3] == False):
            return False
        distance_covered_with_turbo = self.turbo_info[0] * self.turbo_info[1] * 7.7
        remianing_distance = total_length - distance_covered_with_turbo
        return remianing_distance >= -10

    def handle_dynamic_switching(self, track_layout, car_manager):
        # dynamic switching is only valid if there is more than 1 car
        if len(car_manager.cars) >= 1:
            my_car = car_manager.find_my_car()
            switch_piece_index = my_car.car_info.piece_index + 1 if my_car.car_info.piece_index + 1 < len(track_layout.pieces) else 0
            switch_piece = track_layout.pieces[switch_piece_index]
            nb_lanes = len(track_layout.lanes)
            if not self.is_piece_length_travelled(track_layout, my_car, 0.7) and switch_piece.switch:
                cars_in_my_lane = list(itertools.ifilter(lambda c: c.car_info.end_lane == my_car.car_info.end_lane and c.id != my_car.id, car_manager.cars.values()))
                if len(cars_in_my_lane) > 0:
                    cars_in_front = list(itertools.ifilter(lambda c: c.car_info.piece_index > my_car.car_info.piece_index and c.car_info.piece_index - my_car.car_info.piece_index < MIN_DISTANCE_TO_CAR, cars_in_my_lane))
                    if len(cars_in_front) > 0:
                        # try switching to the outer lane first otherwise switch to inner lane
                        if switch_piece.angle > 0:
                            self.switch_pending = 'Left' if my_car.car_info.end_lane > 0 else 'Right'
                        else:
                            self.switch_pending = 'Left' if my_car.car_info.end_lane >= nb_lanes - 1 else 'Right'
                        print 'Dynamic Switching: %s'%(self.switch_pending)

    def record_crash(self):
        # disable the turbo when crashed
        self.in_trubo = False
        self.slip_angle = self.my_car_copy.car_info.angle
        if self.crash_history.has_key(self.transition):
            self.crash_history[self.transition] = self.crash_history[self.transition] + 1
        else:
            self.crash_history[self.transition] = 1

    def get_crash_occurrences(self):
        return self.crash_history.get(self.transition, 0)

    def update_turbo_status(self, value):
        self.in_trubo = value
        if not value:
            self.reset_turbo_crash = False

    def turbo_ticks_elapsed(self, tick_percentage):
        if self.in_trubo:
            ticks = tick_percentage * self.turbo_info[0]
            return self.current_tick > self.turbo_info[2] + ticks
        return False