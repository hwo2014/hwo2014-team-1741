import json
import socket
import sys

from messagetypes import *
from tracklayout import TrackLayout

class Stomper(object):

    def __init__(self, socket, name, key):
        self.socket = socket
        self.name = name
        self.key = key

        # map message types with methods
        self.incoming_msg_map = {
            MSG_JOIN: self.on_join,
            MSG_YOUR_CAR: self.on_your_car,
            MSG_GAME_INIT: self.on_game_init,
            MSG_GAME_START: self.on_game_start,
            MSG_CAR_POSITIONS: self.on_car_positions,
            MSG_TOURNAMENT_END: self.on_tournament_end,
            MSG_CRASH: self.on_crash,
            MSG_GAME_END: self.on_game_end,
            MSG_SPAWN: self.on_spawn,
            MSG_LAP_FINISHED: self.on_lap_finished,
            MSG_DNF: self.on_dnf,
            MSG_FINISH: self.on_finish,
            MSG_ERROR: self.on_error,
            MSG_TURBO_AVAILABLE: self.on_turbo_available,
            MSG_TURBO_START: self.on_turbo_start,
            MSG_TURBO_END: self.on_turbo_end,
            MSG_JOIN_RACE: self.on_join_race
        }

        self.tracklayout = TrackLayout()

    def msg(self, msg_type, data):
        self.send(json.dumps({"msgType": msg_type, "data": data}))

    def send(self, msg):
        self.socket.send(msg + "\n")

    def join(self, track_name = ''):
        if len(track_name) == 0:
            return self.msg(MSG_JOIN, {"name": self.name, "key": self.key})
        else:
            return self.msg(MSG_JOIN_RACE, {"botId":{"name": self.name, "key": self.key}, "trackName": track_name, "carCount": 1})

    def throttle(self, throttle, game_tick):
        self.send(json.dumps({"msgType": MSG_THROTTLE, "data": throttle, "gameTick": game_tick}))

    def turbo(self):
        self.msg(MSG_TURBO, 'GoGoGo')

    def switch_lane(self, direction):
        self.msg(MSG_SWITCH_LANE, direction)

    def ping(self):
        if not self.tracklayout.is_qualifying:
            self.msg(MSG_PING, {})

    def run(self):
        # self.join('keimola')
        # self.join('germany')
        # self.join('usa')
        # self.join('france')
        self.join()
        self.msg_loop()

###################################################
#####	Server callback messages       ############
###################################################

    def on_join(self, data, game_id = '', game_tick = 0):
        print("Joined")
        self.ping()

    def on_join_race(self, data, game_id = '', game_tick = 0):
        print "Joined Race: %s"%(data)
        self.ping()

    def on_your_car(self, data, game_id = '', game_tick = 0):
        print 'My Car Info: %s'%(data)
        self.tracklayout.set_my_car(data)
        self.ping()

    def on_game_init(self, data, game_id = '', game_tick = 0):
        print "Track Layout: %s"%(data)
        self.tracklayout.parse_track_data(data)
        self.ping()

    def on_game_start(self, data, game_id = '', game_tick = 0):
        print("Race Started")
        self.ping()

    def on_tournament_end(self, data, game_id = '', game_tick = 0):
        print "Tournament Ended: %s"%(data)
        self.ping()

    def on_car_positions(self, data, game_id = '', game_tick = 0):
        calculated_throttle = self.tracklayout.update_car_positions(data, game_id, game_tick)

        if calculated_throttle == 2:
            self.switch_lane('Right')
        elif calculated_throttle < 0:
            self.switch_lane('Left')
        elif calculated_throttle == 100:
            self.turbo()
        else:
            self.throttle(calculated_throttle, game_tick)

    def on_crash(self, data, game_id = '', game_tick = 0):
        print "Car Crashed: %s, on game tick: %s"%(data, game_tick)
        self.tracklayout.handle_car_crash(data, game_tick)
        self.ping()

    def on_spawn(self, data, game_id = '', game_tick = 0):
        print "Car Spawned: %s, on game tick: %s"%(data, game_tick)
        self.tracklayout.handle_car_spawn(data, game_tick)
        self.ping()

    def on_turbo_available(self, data, game_id = '', game_tick = 0):
        print "Turbo Available: %s, on game tick: %s"%(data, game_tick)
        self.tracklayout.handle_turbo(data, game_tick)
        self.ping()

    def on_turbo_start(self, data, game_id = '', game_tick = 0):
        print "Turbo Started: %s, on game tick: %s"%(data, game_tick)
        self.tracklayout.update_turbo_status(True)
        self.ping()

    def on_turbo_end(self, data, game_id = '', game_tick = 0):
        print "Turbo Ended: %s, on game tick: %s"%(data, game_tick)
        self.tracklayout.update_turbo_status(False)
        self.ping()

    def on_game_end(self, data, game_id = '', game_tick = 0):
        print "Race Ended: %s"%(data)
        #TODO: handle game end message if necessary
        self.ping()

    def on_lap_finished(self, data, game_id = '', game_tick = 0):
        print "Lap Finished: %s, on game tick: %s"%(data, game_tick)
        self.tracklayout.handle_lap_finished(data, game_tick)
        self.ping()

    def on_dnf(self, data, game_id = '', game_tick = 0):
        print "Car DNF: %s, on game tick: %s"%(data, game_tick)
        self.tracklayout.handle_car_dnf(data, game_tick)
        self.ping()

    def on_finish(self, data, game_id = '', game_tick = 0):
        print "Car Finished Race: %s, on game tick: %s"%(data, game_tick)
        self.tracklayout.handle_car_finished(data, game_tick)
        self.ping()

    def on_error(self, data, game_id = '', game_tick = 0):
        print("Error: {0}".format(data))
        self.ping()

###########################################
# Main game loop

    def msg_loop(self):
        socket_file = s.makefile()
        line = socket_file.readline()
        while line:
            msg = json.loads(line)
            msg_type, data, game_id, game_tick = msg['msgType'], msg['data'], msg.get('gameId', 0), msg.get('gameTick', 0)
            if msg_type in self.incoming_msg_map:
                self.incoming_msg_map[msg_type](data, game_id, game_tick)
            else:
                print("Unhandled message type: {0}".format(msg_type))
                self.ping()
            line = socket_file.readline()


if __name__ == "__main__":
    if len(sys.argv) != 5:
        print("Usage: ./run host port botname botkey")
    else:
        host, port, name, key = sys.argv[1:5]
        print("Connecting with parameters:")
        print("host={0}, port={1}, bot name={2}, key={3}".format(*sys.argv[1:5]))
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((host, int(port)))
        bot = Stomper(s, name, key)
        bot.run()
