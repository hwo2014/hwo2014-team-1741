import json

class CarManager(object):
    def __init__(self):
        self.cars = {}
        self.my_car = ()

    def fill_car_info(self, cars):
        for car in cars:
            car_id = car.get('id')
            car_dimensions = car.get('dimensions')
            race_car = Car(car_id['name'], car_id['color'], car_dimensions['length'], car_dimensions['width'], car_dimensions['guideFlagPosition'])
            self.cars[race_car.id] = race_car

    def set_my_car(self, data):
        self.my_car = (data['name'], data['color'])

    def update_car_info(self, data):
        for d in data:
            car_id = (d['id']['name'], d['id']['color'])
            car = self.find_car_by_id(car_id)
            if car is not None:
                car.car_info.angle = d['angle']
                car.car_info.piece_index = d['piecePosition']['pieceIndex']
                car.car_info.piece_distance = d['piecePosition']['inPieceDistance']
                car.car_info.start_lane = d['piecePosition']['lane']['startLaneIndex']
                car.car_info.end_lane = d['piecePosition']['lane']['endLaneIndex']
                car.car_info.lap = d['piecePosition']['lap']
                self.cars[car_id] = car
            else:
                print 'UNIDENTIFIED CAR: %s'(car_id)

    def handle_car_crash(self, crash_info, game_tick):
        car_id = (crash_info['name'], crash_info['color'])
        crashed_car = self.find_car_by_id(car_id)
        if crashed_car is not None:
            crashed_car.car_info.tick = game_tick
            crashed_car.car_info.is_crashed = True
            self.cars[car_id] = crashed_car

    def handle_car_spawn(self, spawn_info, game_tick):
        car_id = (spawn_info['name'], spawn_info['color'])
        spawned_car = self.find_car_by_id(car_id)
        if spawned_car is not None:
            spawned_car.car_info.tick = game_tick
            spawned_car.car_info.is_crashed = False
            self.cars[car_id] = spawned_car

    def handle_car_dnf(self, dnf_info, game_tick):
        car_id = (dnf_info['car']['name'], dnf_info['car']['color'])
        dnf_car = self.find_car_by_id(car_id)
        if dnf_car is not None:
            dnf_car.car_info.tick = game_tick
            dnf_car.car_info.dnf = True
            self.cars[car_id] = dnf_car

    def handle_car_finished(self, finish_info, game_tick):
        car_id = (finish_info['name'], finish_info['color'])
        finish_car = self.find_car_by_id(car_id)
        if finish_car is not None:
            finish_car.car_info.tick = game_tick
            finish_car.car_info.finished = True
            self.cars[car_id] = finish_car

    def handle_lap_finished(self, data, game_tick):
        car_id = (data['car']['name'], data['car']['color'])
        lap_finish_car = self.find_car_by_id(car_id)
        if lap_finish_car is not None:
            lap_finish_car.car_info.current_lap = data['lapTime']['lap'] + 1
            self.cars[car_id] = lap_finish_car

    def find_car_by_id(self, car_id):
        if self.cars.has_key(car_id):
            return self.cars[car_id]

        return None

    def find_my_car(self):
        return self.find_car_by_id(self.my_car)


class Car(object):
    def __init__(self, name, color, width, length, flag_position):
        self.id = (name, color)
        self.width = width
        self.length = length
        self.guide_flag_position = flag_position

        self.car_info = CarInfo()

    def __str__(self):
        return 'Car=> Id: %s, Width: %s, Length: %s, GuidePosition: %s, CarInfo: %s'%(self.id, self.width, self.length, self.guide_flag_position, self.car_info)

class CarInfo(object):
    def __init__(self):
        self.angle = 0
        self.piece_index = 0
        self.piece_distance = 0
        self.start_lane = -1
        self.end_lane = -1
        self.lap = -1
        self.tick = 0
        self.dnf = False
        self.finished = False
        self.is_crashed = False
        self.current_lap = 0

    def __str__(self):
        return 'Car Info=> Angle: %s, PieceIndex: %s, PieceDistance: %s, StartLane: %s, EndLane: %s, Lap: %s, Tick: %s, Dnf: %s, Finished: %s, IsCrashed: %s'%(self.angle, self.piece_index, self.piece_distance, self.start_lane, self.end_lane, self.lap, self.tick, self.dnf, self.finished, self.is_crashed)